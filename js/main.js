// Init the Bootstrap 3 tooltip thing
$('[data-toggle="tooltip"]').tooltip();

// Hover overlay on article list
$('body').on('mouseenter', '.post-image .overlay', function () {
    $(this).css('opacity', '0.75');
}).on('mouseleave', '.post-image .overlay', function () {
    $(this).css('opacity', '0');
});

$(document).ready(function () {

    function updateArticleList(action, event) {
        event.preventDefault();
        $.ajax({
            url: ajaxurl,
            data: {
                'action': action //
            },
            success: function (data) {
                // This outputs the result of the ajax request
                $('#article-list').html(data);
            },
            error: function (errorThrown) {
                console.log(errorThrown);
            },
            beforeSend: function() {
                $("#article-list").fadeOut("slow");
            },
            complete: function() {
                $("#article-list").fadeIn("slow");
            }
        });
    }

    $('#sortbar-most-popular').click(function (e) {
        updateArticleList('most_popular_articles', e);
    });

    $('#sortbar-most-commented').click(function (e) {
        updateArticleList('most_commented_articles', e);
    });

    $('#sortbar-most-recent').click(function (e) {
        updateArticleList('most_recent_articles', e);
    });

});

