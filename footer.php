<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2014/12/18
 * @version 1
 */
?>

<hr>

<!-- Footer -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 text-center">
                <p class="copyright text-muted">Copyright &copy; MaghiaRomania.ro <?= date('Y')?></p>
                <img src="<?= get_template_directory_uri(); ?>/img/logo.png" alt="MaghiaRomania logo" title="MaghiaRomania" class="footer-logo">
			</div>
		</div>
	</div>
</footer>

<!-- jQuery -->
<script src="<?= get_template_directory_uri(); ?>/prod.js"></script>
<script type="application/ld+json">
      {
      "@context": "http://schema.org/",
      "@type": "Organization",
      "url": "http://www.<?= $_SERVER['HTTP_HOST']; ?>",
      "logo": "<?= get_template_directory_uri(); ?>/img/logo.png"
      }

</script>
<?php wp_footer(); ?>
</body>

</html>
