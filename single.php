<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2014/12/18
 * @version 1
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
    <div class="visible-print">
        <?php
        the_post_thumbnail('full', ['itemprop' => 'image']);
        ?>
    </div>
<?php if (has_post_thumbnail()) : ?>
    <header class="intro-header" style="background-image: url('<?= $url ?>')">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-heading">
                        <h1><span><?= get_the_title(); ?></span></h1>
                        <hr class="small">
                        <span class="subheading">
                            <span>
                                <?= get_the_excerpt(); ?>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php else: ?>
    <header class="intro-header" style="background: #7d7d7d">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="site-heading">
                        <h1><span><?= get_the_title(); ?></span></h1>
                        <hr class="small">
                        <span class="subheading">
                            <span>
                                <?= get_the_excerpt(); ?>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </header>
<?php endif; ?>

    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-lg-offset-1">
                <p class="article-info">
                    <i class="fa fa-folder-open-o"></i> <?php foreach (get_the_category() as $category) : ?><?= $category->name; ?>, &nbsp;<?php endforeach; ?>
                    <time datetime="<?php the_date('c'); ?>" itemprop="datePublished" pubdate>
                        <i class="fa fa-calendar-o"></i> <?= get_the_date(); ?>&nbsp;
                    </time>
                    <i class="fa fa-comments-o"></i> <?= get_comment_count()['approved']; ?>
                </p>
                <article itemprop="mainContentOfPage" <?php post_class("single-article"); ?>>
                    <?php the_content(); ?>
                </article>
                <div class="tag-links">
                    <?php $posttags = get_the_tags(); ?>
                    <?php if ($posttags) : foreach ($posttags as $tag) : ?>
                        <a href="#"><?= $tag->name; ?></a>
                    <?php endforeach; endif; ?>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="author left-brand-border">
                    <div class="col-xs-4">
                        <img src="<?= get_avatar_url($post->post_author) ?>" class="img-responsive">
                    </div>
                    <div class="col-xs-8">
                        <h5><?= get_the_author(); ?></h5>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="spacer15"></div>

                <div class="like-box">
                    <div class="col-xs-12 left-brand-border">
                        <div class="fb-like" data-href="http://<?= $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"
                             data-layout="button_count" data-action="like" data-show-faces="false"
                             data-share="true"></div>
                    </div>
                </div>

                <div class="spacer15"></div>

                <div class="tag-cloud left-brand-border">
                    <div class="col-xs-12">
                        <div class="tag-links">
                            <?php $posttags = get_the_tags(); ?>
                            <?php if ($posttags) : foreach ($posttags as $tag) : ?>
                                <a href="#"><?= $tag->name; ?></a>
                            <?php endforeach; endif; ?>
                        </div>
                        <div class="spacer10"></div>

                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="spacer15"></div>

                <div class="facebook-feed left-brand-border">
                    <div class="col-xs-12">
                        <?php get_template_part('parts/facebook-feed'); ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
<?php get_footer(); ?>