<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/07
 * @version 1
 */
require_once('includes/custom-comment.php');      // Custom comment template
require_once('includes/menus.php');               // Theme menus
require_once('lib/wp_bootstrap_navwalker.php');   // Register Custom Bootstrap 3 Navigation Walker
require_once('includes/thumbnails.php');          // Thumbnails and related stuff
require_once('lib/html_tag_schema.php');          // Schema.org stuff (SEO Stuff)
require_once('includes/custom_login.php');        // Custom login
require_once('includes/theme-options.php');       // Theme options
//require_once( 'includes/maintenance-mode.php' );    // Maintenance mode
require_once('includes/front-page-articles.php'); // Front page article sort
require_once('includes/post-views.php');          // Set post view count

add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('html5', ['comment-list', 'comment-form', 'search-form', 'gallery', 'caption']);

// Add Bootstrap 3 specific class to the pagination
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

function posts_link_attributes()
{
    return 'class="btn btn-primary"';
}

function image_tag_class($class)
{
    $class .= ' img-responsive';

    return $class;
}

add_filter('get_image_tag_class', 'image_tag_class');


// Disable loading jQuery, because we are loading jQuery via the prod.js
add_action('wp_enqueue_scripts', 'no_more_jquery');
function no_more_jquery()
{
    wp_deregister_script('jquery');
}

function getContributors()
{
    global $wpdb;

    $authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users WHERE display_name <> 'mrbig00' ORDER BY display_name");

    return $authors;
}

/* Change Excerpt length */
function custom_excerpt_length($length)
{
    return 4;
}

add_filter('excerpt_length', 'custom_excerpt_length', 5);

function my_init()
{
    add_post_type_support('page', ['excerpt']);
}

add_action('init', 'my_init');

function new_excerpt_more( $more ) {
    return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');