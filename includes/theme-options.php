<?php
/**
 * @author      Zoltan Szanto <mrbig00@gmail.com>
 * @since       2015/01/22
 * @version     1
 * @description Add a theme option page to the admin area
 */

//
add_action( 'admin_menu', 'mgr_create_menu' );
function mgr_create_menu() {

	//create new top-level menu
	add_menu_page(
		'Theme Settings',
		'Theme Settings',
		'administrator',
		'mgr-theme-settings',
		'mgr_settings_page',
		'dashicons-megaphone'
	);
}

/**
 * Step 2: Create settings fields.
 */
add_action( 'admin_init', 'register_mgrsettings' );

function register_mgrsettings() {
	register_setting( 'mgr-settings-general', 'mgr_site-in-maintenance' );
}

/**
 * Step 3: Create the markup for the options page
 */
function mgr_settings_page() {

	?>

	<div class="wrap">
		<h2>Theme settings</h2>

		<form method="post" action="options.php">

			<?php if ( isset( $_GET['settings-updated'] ) ) { ?>
				<div class="updated">
					<p>Settings updated successfully</p>
				</div>
			<?php } ?>

			<table class="form-table">
				<tr>
					<td colspan="2"><h3>General settings</h3></td>
				</tr>

				<tr valign="top">
					<th scope="row">
						<label for="mgr_site-in-maintenance">Site in maintenance mode</label>
					</th>
					<td>
						<select id="mgr_site-in-maintenance" name="mgr_site-in-maintenance" style="width: 50%">
							<option value="1" <?= (get_option( 'mgr_site-in-maintenance' ) == "1") ? "selected" : ""; ?>>
							Yes
							</option>
							<option value="0"  <?= (get_option( 'mgr_site-in-maintenance' ) == "0") ? "selected" : ""; ?>>No</option>
						</select>
						<p class="description">Display a maintenance mode page for non-admin users</p>
					</td>
				</tr>

				<?php settings_fields( 'mgr-settings-general' ); ?>
				<?php do_settings_sections( 'mgr-settings-general' ); ?>
			</table>
			<?php submit_button(); ?>
		</form>
	</div>
<?php }