<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/23
 * @version 1
 */


// Most popular articles
function most_popular_articles() {


	$posts = new WP_Query( array( 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC' ) );
	while ( $posts->have_posts() ) : $posts->the_post();
		get_template_part( 'parts/archive', 'post' );
	endwhile;

	die();
}

add_action( 'wp_ajax_most_popular_articles', 'most_popular_articles' );
add_action( 'wp_ajax_nopriv_most_popular_articles', 'most_popular_articles' );


// Most commented articles
function most_commented_articles() {
	$posts = new WP_Query( array( 'orderby' => 'comment_count', 'order' => 'DESC' ) );
	while ( $posts->have_posts() ) : $posts->the_post();
		get_template_part( 'parts/archive', 'post' );
	endwhile;

	die();
}

add_action( 'wp_ajax_most_commented_articles', 'most_commented_articles' );
add_action( 'wp_ajax_nopriv_most_commented_articles', 'most_commented_articles' );

// Most recent articles
function most_recent_articles() {
	$posts = new WP_Query( array( 'orderby' => 'date', 'order' => 'DESC' ) );
	while ( $posts->have_posts() ) : $posts->the_post();
		get_template_part( 'parts/archive', 'post' );
	endwhile;

	die();
}

add_action( 'wp_ajax_most_recent_articles', 'most_recent_articles' );
add_action( 'wp_ajax_nopriv_most_recent_articles', 'most_recent_articles' );
