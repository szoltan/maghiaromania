<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/23
 * @version 1
 */


function maintenace_mode() {
	$current_user = wp_get_current_user();
	if ( ! user_can( $current_user, 'administrator' ) && ( get_option( 'mgr_site-in-maintenance' ) == 1 ) ) {
		get_template_part( 'parts/maintenance' );
		die();
	}
}

add_action( 'get_header', 'maintenace_mode' );