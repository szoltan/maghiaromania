<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/13
 * @version 1
 */

// Add Bootstrap 3's img-responsive class to the_post_thumbnail
function alter_attr_wpse_102158( $attr ) {
	remove_filter( 'wp_get_attachment_image_attributes', 'alter_attr_wpse_102158' );
	$attr['class'] .= ' img-responsive';

	return $attr;
}

add_filter( 'wp_get_attachment_image_attributes', 'alter_attr_wpse_102158' );

// Support for post thumbnails
add_theme_support( 'post-thumbnails' );

// Thumbnail sizes
add_image_size( 'archive-article', 1000, 480, true );
add_image_size( 'header-image', 1900, 872, true );