<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/20
 * @version 1
 */

function custom_login() {
	?>
	<style type="text/css">
		body.login div#login h1 a {
			background-image: url(<?= get_stylesheet_directory_uri(); ?>/img/logo.png);
			padding-bottom: 0;
			width: 90px;
		}
	</style>
<?php
}