<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/13
 * @version 1
 */

global $facebookFeed;
const FB_PAGE_ID      = '142530262436733';
const FB_APP_ID       = '817840434941328';
const FB_APP_SECRET   = '60034604fb3d3adde4be1c1fa042c27b';
const FB_ACCESS_TOKEN = '142530262436733|60034604fb3d3adde4be1c1fa042c27b';

require_once(realpath(dirname(__FILE__)). '/../lib/facebook/src/facebook.php' );



function initFacebook() {
	// connect to app
	$config               = array();
	$config['appId']      = FB_APP_ID;
	$config['secret']     = FB_APP_SECRET;
	$config['fileUpload'] = false;

	return new Facebook( $config );
}

function getFacebookFeed() {

	$facebook = initFacebook();
	$fields = "id,message,picture,link,name,description,type,icon,created_time,from,object_id,actions,story,source";
	$limit = 5;
	return $facebook->api( "/" . FB_PAGE_ID . "/feed?limit=$limit&fields=$fields" );
}

add_action( 'init', 'globalizeFacebookFeed' );

function globalizeFacebookFeed() {
	global $facebookFeed;
	$feed = getFacebookFeed();
	if ( isset( $feed['data'] ) ) {
		$facebookFeed = $feed['data'];
	} else {
		$facebookFeed = array();
	}
}