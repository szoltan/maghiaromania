<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/13
 * @version 1
 */
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'Primary Menu' ),
) );