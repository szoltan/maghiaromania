<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2014/12/18
 * @version 1
 */

?>
<?php get_header(); ?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('<?= get_template_directory_uri(); ?>/img/home-bg.png')">
	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				<div class="site-heading" style="opacity: 0; padding: 300px 0">
					<h1>MaghiaRomania</h1>
					<hr class="small">
					<span class="subheading">Despre minoritati si alti maghiari</span>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- Main Content -->
<div class="container">
	<div class="row">
		<div class="col-lg-9">
			<main id="article-list">
				<?php
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						get_template_part( 'parts/archive', 'post' );
					}
				}
				?>
			</main>
			<?php posts_nav_link( 'separator', 'Go backward', 'Go forward' ); ?>
		</div>
		<section id="sidebar">
			<?php get_template_part( 'parts/archive', 'sidebar' ) ?>
		</section>
	</div>
</div>
<?php get_footer(); ?>
