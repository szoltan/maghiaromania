<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/09
 * @version 1
 */
?>
<div class="social left-brand-border">
	<div class="col-xs-12 text-center">
		<a href="#" data-toggle="tooltip" data-placement="bottom" title="Sign up to the newsletter">
			<i class="fa fa-envelope-o fa-2x"></i>
		</a>

		<a href="#">
			<i class="fa fa-facebook-square fa-2x" data-toggle="tooltip" data-placement="bottom" title="Follow us on facebook"></i>
		</a>

		<a href="#">
			<i class="fa fa-twitter-square fa-2x" data-toggle="tooltip" data-placement="bottom" title="Follow us on twitter"></i>
		</a>

		<form>
			<input type="text" class="form-control pull-right" placeholder="cautare..." name="srch-term">
		</form>

	</div>
	<div class="clearfix"></div>
</div>