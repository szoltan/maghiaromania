<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/23
 * @version 1
 */
?>
<div class="row">
	<div class="col-sm-offset-6 col-sm-6 col-xs-12">
		<div class="bottom-brand-border">
			<div class="pull-left">
				Ordonează după
			</div>
			<div class="pull-right">

				<a class="btn btn-primary btn-sm" href="#" data-toggle="tooltip" data-placement="bottom" title="Cele mai populare" id="sortbar-most-popular">
					<i class="fa fa-user-times"></i>
				</a>

				<a class="btn btn-primary btn-sm" href="#" data-toggle="tooltip" data-placement="bottom" title="Cele mai comentate" id="sortbar-most-commented">
					<i class="fa fa-comments"></i>
				</a>

				<a class="btn btn-primary btn-sm" href="#" data-toggle="tooltip" data-placement="bottom" title="Cele mai recente" id="sortbar-most-recent">
					<i class="fa fa-clock-o"></i>
				</a>

			</div>
			<div class="spacer10 clearfix"></div>
		</div>
	</div>
</div>
<div class="spacer15"></div>