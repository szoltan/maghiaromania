<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/08
 * @version 1
 */
?>
<div class="col-lg-3">
    <?php get_template_part('parts/sidebar/authors-list'); ?>
    <div class="spacer15"></div>

    <div class="tag-cloud left-brand-border">
        <div class="col-xs-12">
            <div class="tag-links">
                <h4>Scriem despre</h4>
                <?php $tags = get_tags(['orderby' => 'count', 'order' => 'desc', 'number' => 30]); ?>
                <?php if ($tags) : foreach ($tags as $tag) : ?>
                    <a href="<?= get_tag_link($tag->term_id); ?>"><?= $tag->name; ?></a>
                <?php endforeach; endif; ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="spacer15"></div>

    <div class="facebook-feed left-brand-border">
        <div class="col-xs-12">
            <?php get_template_part('parts/facebook-feed'); ?>
        </div>
        <div class="clearfix"></div>
    </div>


</div>
