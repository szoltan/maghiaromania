<?php
/**
 * @package maghiaromania.
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2017/02/15
 */
?>
<div class="authors left-brand-border">
    <div class="col-xs-12">
        <h4>Maghiaromânii</h4>

        <?php $contributors = getContributors(); ?>

        <div class="row">
            <?php foreach ($contributors as $contributor): ?>
                <div class="col-xs-3">
                    <a title="Articole scris de <?= $contributor->user_nicename; ?>"
                        href="<?= get_author_posts_url($contributor->ID); ?>"
                    >
                        <img src="<?= get_avatar_url($contributor->ID) ?>"
                             class="img-responsive"
                             alt="<?= $contributor->user_nicename ?>">
                    </a>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
