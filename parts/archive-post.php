<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/07
 * @version 1
 */
?>
<div class="post-preview">

    <a href="<?= get_the_permalink(); ?>">
        <?php if (has_post_thumbnail()): ?>
        <div class="post-image" style="position: relative; overflow: hidden">
            <?php the_post_thumbnail('archive-article'); ?>
            <div class="overlay"
                 style="bottom: 0; position: absolute; width: 100%; height: 100%; background: #000000; transition: .2s; opacity: 0">
                <div style="position: relative; width: 100%; height: 100%; opacity: 1; color: #fff">
                    <div style="top: 45%; position: absolute; text-align: center; width: 100%; opacity: 1">
                        <span><i class="fa fa-comments-o fa-2x"></i> &nbsp;<?= get_comments_number(); ?></span> <br><br>
                        <p>
                            <?php $posttags = get_the_tags(); ?>
                            <?php if ($posttags) : foreach ($posttags as $tag) : ?>
                                #<?= $tag->name; ?>,
                            <?php endforeach;
                            endif; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <p class="article-tags">
            <?php foreach (get_the_category() as $category) : ?>
                <?= $category->name; ?>, &nbsp;
            <?php endforeach; ?>
        </p>
        <?php if (!has_post_thumbnail()): ?>
            <br>
        <?php endif; ?>
        <h2 class="post-title">
            <?= get_the_title(); ?>
        </h2>

        <p class="post-subtitle">
            <?php the_excerpt(); ?>
        </p>
    </a>

    <p class="post-meta">
        Semnat de
        <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>">
            <?= get_the_author(); ?>
        </a>
        la data de <?= get_the_date(); ?>
    </p>
</div>
<hr>