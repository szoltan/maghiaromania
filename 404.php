<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2015/01/14
 * @version 1
 */
?>
<?php get_header(); ?>
<header class="intro-header" style="background-image: url('<?= get_template_directory_uri(); ?>/img/home-bg.png')">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
				<div class="site-heading" style="padding: 300px 0">
					<h1>Awww snap!</h1>
					<hr class="small">
					<span class="subheading">We can't find this content.</span>
				</div>
			</div>
		</div>
	</div>
</header>
<?php get_footer(); ?>
