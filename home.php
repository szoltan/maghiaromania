<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2014/12/18
 * @version 1
 */

?>
<?php get_header(); ?>

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('<?= get_template_directory_uri(); ?>/img/home-bg.svg')">

</header>
<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <section id="sort-bar">
                <?php get_template_part('parts/home', 'sortbar'); ?>
            </section>
            <main id="article-list">
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        get_template_part('parts/archive', 'post');
                    }
                }
                ?>
            </main>
            <div class="row">
                <div class="col-sm-6">
                    <?php previous_posts_link(); ?>
                </div>
                <div class="col-sm-6 text-right">
                    <?php next_posts_link(); ?>

                </div>
            </div>
        </div>
        <section id="sidebar">
            <?php get_template_part('parts/archive', 'sidebar') ?>
        </section>
    </div>
</div>
<?php get_footer(); ?>
