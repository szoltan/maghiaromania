module.exports = (grunt) ->
  grunt.initConfig
  # dump sql dbs
    db_dump:
      options: {}

    # common options should be defined here

    # "Local" target
      local:
        options:
          title: "Local DB"
          database: "maghiaromania"
          user: "root"
          pass: ""
          host: "127.0.0.1"
          backup_to: "db/backups/local/local_" + new Date().toISOString() + ".sql"

      stage:
        options:
          title: "Staging DB"
          database: "maghiaro_staging"
          user: "maghiaro_staging"
          pass: "Ergo1357!"
          host: "maghiaromania.ro"
          backup_to: "db/backups/staging/staging" + new Date().toISOString() + ".sql"

  # Copy Font Awesome files to  /fonts directory
    copy:
      fonts:
        files: [
          expand: true
          cwd: 'bower_components/font-awesome/fonts'
          src: ['**/*']
          dest: 'fonts'
        ]

  # Concatenate all JavaScript files in the /js directory
    concat:
      js:
        options:
          separator: ";\n"
        src: [
          'bower_components/jquery/dist/jquery.js'
          'bower_components/bootstrap/dist/js/bootstrap.min.js'
          'bower_components/bootstrap-material-design/scripts/material.js'
          'bower_components/bootstrap-material-design/scripts/ripples.js'
          'js/**/*.js'
        ]
        dest: 'prod.js'

  # Minify the concatenated JavaScript output
    uglify:
      options:
        mangle: false,
        sourceMap: false
      js:
        files:
          'prod.js': ['prod.js']

  # Compile LESS stylesheet into /style.css
    less:
      style:
        options:
          sourceMap: true
          sourceMapFilename: 'style.css.map'
          sourceMapURL: 'style.css.map'
          cleancss: false
      # report: "min"

        files:
          "style.css": "less/style.less"

  # During developement, watch files and recompile/reload
    watch:
      php:
        files: ['**/*.php']
        tasks: []
        options:
          livereload: false
      js:
        files: ['js/**/*.js']
        tasks: ['concat:js', 'uglify:js']
        #tasks: ['concat:js']
        options:
          livereload: false
      less:
        files: ['less/**/*.less']
        tasks: ['less:style']
        options:
          livereload: false

    'ftp-deploy':
        build:
          auth:
            host: 'maghiaromania.ro',
            port: 21,
            authKey: 'staging'

          src: './',
          dest: '/',
          exclusions: ['bower_components', 'node_modules', '.idea', 'db']



  # Build theme
  grunt.registerTask 'build', [
    'copy'
    'concat:js'
    #'uglify:js'
    'less:style'
  ]

  # Build theme and start watching files (for development)
  grunt.registerTask 'dev', [
    'build'
    'watch'
  ]

  grunt.registerTask 'db', [
    'db_dump'
  ]

  # Load grunt modules
  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-less')
  grunt.loadNpmTasks('grunt-contrib-uglify')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-mysql-dump')
  grunt.loadNpmTasks('grunt-ftp-deploy')