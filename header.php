<?php
/**
 * @author  Zoltan Szanto <mrbig00@gmail.com>
 * @since   2014/12/18
 * @version 1
 */
?>
<!DOCTYPE html>
<html <?php html_tag_schema(); ?> <?php language_attributes(); ?>>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="<?= get_template_directory_uri(); ?>/img/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="<?= get_template_directory_uri(); ?>/img/favicon//apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri(); ?>/img/favicon/apple-touch-icon-152x152.png" />
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link href="<?= get_template_directory_uri(); ?>/style.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
		var ajaxurl = "<?= admin_url('admin-ajax.php'); ?>";
	</script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1428567394092918&version=v2.0";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
<?php $navbarMarginTop = ( is_admin_bar_showing() ) ? "32px" : "0"; ?>
<nav class="navbar navbar-default navbar-custom navbar-fixed-top" style="margin-top: <?= $navbarMarginTop; ?>">
	<div class="container-fluid">
		<div class="navbar-header page-scroll">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/">
				<img class="navbar-brand-img" src="<?= get_template_directory_uri(); ?>/img/logo.png" style="float: left;">
				<span class="pull-left">MaghiaRom&acirc;nia</span>
			</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php
			wp_nav_menu( array(
					'menu'            => 'primary',
					'theme_location'  => 'primary',
					'depth'           =>  2,
					'container'       => 'span',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'nav navbar-nav',
					'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
					'walker'          => new wp_bootstrap_navwalker(),
				)
			);
			?>
            <!--
			<form class="navbar-form navbar-right" role="search">
				<div class="form-group">
					<input type="text" class="form-control pull-right" placeholder="căutare..." name="srch-term" id="header-search">
				</div>
				<div class="form-group">
                    <a href="mailto:maghiaromania@gmail.com" data-toggle="tooltip" data-placement="bottom" title="Contacteaza-ne">
                        <i class="fa fa-at fa-2x" aria-hidden="true"></i>
                    </a>

					<a href="#" data-toggle="tooltip" data-placement="bottom" title="Înscrie-te la newsletter">
						<i class="fa fa-envelope-o fa-2x"></i>
					</a>

					<a href="https://www.facebook.com/MaghiaRomania">
						<i class="fa fa-facebook-square fa-2x" data-toggle="tooltip" data-placement="bottom" title="Urmărește-ne pe facebook"></i>
					</a>
				</div>
			</form>
			-->
		</div>
	</div>
</nav>